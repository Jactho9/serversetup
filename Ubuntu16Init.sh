#!/bin/bash
echo "Starting Setup!"

echo "Need to know the port, username and password"
read -p "Port (Should be greater than 1024): " port
read -p "User name (Must be all lowercase letters): " name
read -p "Password: " password

printf "\n***Config***\n"
printf "Port: ${port}\nUsername: ${name}\nPassword: ${password}\n"

# need to be root, run this before script: sudo su -

printf "\n***Creating User***\n"
if id -u "$name" >/dev/null 2>&1; then
    echo 'User already exists'
else
    echo 'User is missing. Creating now'
    adduser $name --gecos "First Last,RoomNumber,WorkPhone,HomePhone" --disabled-password
    echo "$name:$password" | chpasswd
    usermod -aG sudo $name
fi


printf "\n***Handling sshd_config***\n"
sed -i "s/#\?\Port 22/Port $port/g" /etc/ssh/sshd_config
sed -ie 's/\(.*\)PermitRootLogin yes/PermitRootLogin no/g' /etc/ssh/sshd_config
sed -ie 's/\(.*\)PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config

if grep -q "AllowUsers $name root" "/etc/ssh/sshd_config"; then
  echo "'AllowUsers $name root' was already in the sshd config file"
else
  echo -e "\nAllowUsers $name root" >> /etc/ssh/sshd_config
fi

if grep -q "DebianBanner no" "/etc/ssh/sshd_config"; then
  echo "'DebianBanner no' was already in the sshd config file"
else
  echo -e "\nDebianBanner no" >> /etc/ssh/sshd_config
fi


printf "\n***Securing shared memory***\n"
if grep -q "tmpfs     /run/shm     tmpfs" "/etc/fstab"; then
  echo "Memory already secured"
else
  echo -e "\ntmpfs     /run/shm     tmpfs     defaults,noexec,nosuid     0     0" >> /etc/fstab
fi

printf "\n***Rebooting. Will now need to login as $name with port $port and run the next script***\n"
sleep 10
sudo reboot