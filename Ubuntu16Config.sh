#!/bin/bash
echo "Starting Config!"

name=$USER
echo "Logged in as $name"
read -p "What is the Port: " port

printf "\n***Add windows as trusted key***\n"
wget https://packages.microsoft.com/config/ubuntu/16.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
sudo dpkg -i packages-microsoft-prod.deb


printf "\n***Update and install asp .net core runtime***\n"
sudo apt-get update; \
  sudo apt-get install -y apt-transport-https && \
  sudo apt-get update && \
  sudo apt-get install -y aspnetcore-runtime-3.1

printf "\n***Installing unzip***\n"
sudo apt install unzip


printf "\n***Installing Nginx***\n"
echo -e "\ndeb https://nginx.org/packages/ubuntu/ Xenial nginx" >> /etc/apt/sources.list
echo -e "\ndeb deb-src https://nginx.org/packages/ubuntu/ Xenial nginx" >> /etc/apt/sources.list
sudo apt-get update
sudo apt-get install nginx

sudo mkdir /home/$name/Hosting

printf "\n***Setting up the firewall***\n"
sudo apt-get install ufw
sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow $port
sudo ufw allow http
sudo ufw allow https
sudo ufw enable
sudo ufw status verbose


printf "\n***Installing certbot***\n"
sudo add-apt-repository ppa:certbot/certbot
sudo apt-get update
sudo apt-get install python-certbot-nginx


sudo bash -c '> /etc/nginx/sites-available/default'



printf "\n***Installing PostgreSql***\n"

sudo apt-get update
# Create the file repository configuration:
sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Import the repository signing key:
wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | sudo apt-key add -

# Update the package lists:
sudo apt-get update

# Install the latest version of PostgreSQL.
# If you want a specific version, use 'postgresql-12' or similar instead of 'postgresql':
sudo apt-get -y install postgresql-13
#answer the bellow is jac then yes
#sudo -u postgres createuser --interactive
sudo -u postgres createuser jac -s 
sudo -u postgres createdb $name
sudo -u $name psql -c "ALTER USER $name PASSWORD 'apptoffie';"
