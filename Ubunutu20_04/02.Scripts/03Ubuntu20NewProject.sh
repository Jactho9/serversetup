#!/bin/bash
echo "Starting New Project!"

name=$USER
echo "Logged in as $name"
read -p "What is the .NET project name (Should start with capital letter): " projectName
read -p "What is the website (e.g site.com): " website
read -p "What port will the site run on? (Can be anything but for simplicity use Application URL in Properties/launchSettings.json): " port


nginxConfig="
server {
    listen        80;
    server_name   $website *.$website;
    location / {
        proxy_pass         http://localhost:$port;
        proxy_http_version 1.1;
        proxy_set_header   Upgrade \$http_upgrade;
        proxy_set_header   Connection keep-alive;
        proxy_set_header   Host \$host;
        proxy_cache_bypass \$http_upgrade;
        proxy_set_header   X-Forwarded-For \$proxy_add_x_forwarded_for;
        proxy_set_header   X-Forwarded-Proto \$scheme;
    }
}"

defaultNginxServer="
server {
    listen   80 default_server;
    # listen [::]:80 default_server deferred;
    return   444;
}"

serviceConfig="
[Unit]
Description=.NET Core React App running on Ubuntu

[Service]
WorkingDirectory=/home/$name/Hosting/$projectName
ExecStart=/usr/bin/dotnet /home/$name/Hosting/$projectName/$projectName.dll --urls=http://localhost:$port/
Restart=always
# Restart service after 10 seconds if the dotnet service crashes:
RestartSec=10
KillSignal=SIGINT
SyslogIdentifier=$projectName
User=$name
Environment=ASPNETCORE_ENVIRONMENT=Production
Environment=DOTNET_PRINT_TELEMETRY_MESSAGE=false

[Install]
WantedBy=multi-user.target
"

sudo mkdir -p /home/$name/Hosting/$projectName


printf "\n***Adding to nginx config***\n"
if grep -q "$website *.$website;" "/etc/nginx/sites-available/default"; then
  echo "nginx already has site"
else
  echo "$nginxConfig" | sudo tee -a /etc/nginx/sites-available/default
fi

if grep -q "default_server deferred" "/etc/nginx/sites-available/default"; then
  echo "nginx already has default deferred site"
else
  echo "$defaultNginxServer" | sudo tee -a /etc/nginx/sites-available/default
fi

printf "\n***Adding service file***\n"
sudo rm -f /etc/systemd/system/kestrel-$projectName.service
echo "$serviceConfig" | sudo tee -a /etc/systemd/system/kestrel-$projectName.service

printf "\n***Setting up SSL***\n"
sudo certbot --nginx -d $website -d www.$website